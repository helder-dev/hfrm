/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hfrm;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.LineNumberReader;

/**
 *
 * @author 1181307
 */
public class Ficheiro {
    
    public Ficheiro() {
    }
    
    private String tempPath = System.getProperty("java.io.tmpdir");
    
    public int linhasFich(String nomeFx) throws IOException{

        InputStream is = new BufferedInputStream(new FileInputStream(nomeFx));
        File f = new File(nomeFx);
        FileReader fr = new FileReader(f);

        int nLinhas= 0;
        if (f.exists() && !f.isDirectory()) {
            LineNumberReader lnr = new LineNumberReader(fr);
            while (lnr.readLine()!= null){
                nLinhas++;
            }
            lnr.close();
            fr.close();
            is.close();

            return nLinhas;

        } else {
            return nLinhas;
        }

    }
    
}
