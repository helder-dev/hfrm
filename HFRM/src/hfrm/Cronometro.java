/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hfrm;

/**
 *
 * @author Goldb
 */
public class Cronometro {
    private long inicio, fim;
 
    public void start(){
        inicio = System.nanoTime();
    }
 
    public void stop(){
        fim = System.nanoTime();
    }
 
    public long getNanoSegundos() {
        return fim-inicio;
    }
}
