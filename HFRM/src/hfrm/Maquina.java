/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hfrm;

import java.util.Comparator;

/**
 *
 * @author 1181539
 */
public class Maquina implements Comparable<Maquina> {
    private String codMaquina;
    private int[] capacidadeProdutiva;
    private int[] producaoDiaria;
    private String path;
    
    public Maquina(String codMaquina, int[] capacidadeProdutiva) {
        this.codMaquina = codMaquina;
        this.capacidadeProdutiva = capacidadeProdutiva;
        this.producaoDiaria = new int[7];
    }
    
    
    public String getCodMaquina(){
        return codMaquina;
    }
    
    public void setCodMaquina(String codMaquina){
        this.codMaquina = codMaquina; 
    }
    
    public int[] getCapacidadeProdutiva(){
        return capacidadeProdutiva;
    }
    
    public int[] getProducaoDiaria(){
        return this.producaoDiaria;
    }
    public int getCapacidadeProdutiva(int i){
        return capacidadeProdutiva[i];
    }
    
    public int getCapacidadeProdutivaByDia(int numDia){
        return capacidadeProdutiva[numDia];
    }
   
    public void setCapacidadeProdutiva(int[] capacidadeProdutiva){
        this.capacidadeProdutiva = capacidadeProdutiva; 
    }
    
    public void setProducaoDiaria(int dia,int producaoDia){
        this.producaoDiaria[dia-1] = producaoDia;
    }
    
    public int getProducaoSegunda(){
        return this.producaoDiaria[0]; 
    }
    
    public int getProducaoTerca(){
        return this.producaoDiaria[1];
    }
    
    public int getProducaoQuarta(){
        return this.producaoDiaria[2];
    }
    
    public int getProducaoQuinta(){
        return this.producaoDiaria[3];
    }
    
    public int getProducaoSexta(){
        return this.producaoDiaria[4];
    }
    
    public int getProducaoSabado(){
        return this.producaoDiaria[5];
    }
    
    public int getProducaoDomingo(){
        return this.producaoDiaria[6];
    }
    
    public static Comparator<Maquina> MaquinaNameComparator = new Comparator<Maquina>() {
        public int compare(Maquina m1, Maquina m2) {
            if (m1 == null && m2 == null) {
                return 0;
            }
            if (m1 == null) {
                return 1;
            }
            if (m2 == null) {
                return -1;
            }
            
          String nomeMaquina1 = m1.getCodMaquina().toUpperCase();
          String nomeMaquina2 = m2.getCodMaquina().toUpperCase();

          return nomeMaquina1.compareTo(nomeMaquina2);
        }
    };

    @Override
    public int compareTo(Maquina o) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }
    
    
    public static Comparator<Maquina> MaquinaProdutividadeSemComparator = new Comparator<Maquina>() {
        
         public int compare(Maquina m1, Maquina m2) {
            if (m1 == null && m2 == null) {
                return 0;
            }
            if (m1 == null) {
                return 1;
            }
            if (m2 == null) {
                return -1;
            }
          int totalCapProd1 = 0;
          int[] capProdM1 = m1.getCapacidadeProdutiva();
          for (int i = 0; i < capProdM1.length; i++) {
              totalCapProd1 = totalCapProd1 + capProdM1[i];
          }
          
          int totalCapProd2 = 0;
          int[] capProdM2 = m2.getCapacidadeProdutiva();
          for (int i = 0; i < capProdM2.length; i++) {
              totalCapProd2 = totalCapProd2 + capProdM2[i];
          }

          return totalCapProd2 - totalCapProd1;
        }
    };
    
    
}

