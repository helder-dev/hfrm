/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hfrm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Formatter;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.lang.String;

/**
 *
 * @author 1181539
 */
public class HFRM {

    private static final Scanner scanner = new Scanner(System.in);
    private static String tempPath = getPath();
    private static Maquina[] maquinas = new Maquina[80];

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        carregarMaquinas();
        preencherProducaoDiaria();
        int opcao;
        do{
            System.out.println("0 - Sair");
            System.out.println("1 - Mostrar informação base de uma máquina");
            System.out.println("2 - Atualizar capacidade produtiva diária de uma máquina");
            System.out.println("3 - Remover informação de uma máquina");
            System.out.println("4 - Guardar informação base de uma máquina");
            System.out.println("5 - Ordenar informação alfabéticamente");
            System.out.println("6 - Ordenar informação por ordem decrescente");
            System.out.println("7 - Inserir produtividade das máquinas por dia da semana");
            System.out.println("8 - Mostrar toda a informação");
            System.out.println("9 - Listar para ficheiro texto máquina prod. semanal inf. a 30%");
            System.out.println("10 - Guardar toda a informação em memória num ficheiro texto");
            System.out.println("11 - Determinar melhor e pior dia de cada máquina");
            System.out.println("12 - Imprimir em ficheiro de texto a produção semanal por tipo de máquina");
            System.out.println("13 - Atualizar a produtividade de uma máquina num dia da semana");
            System.out.println("14 - Recuperar para memória toda a informação guardada no ficheiro");
            opcao = getInt("");

            switch (opcao) {
                case 1:
                    mostrarInformacaoBase();
                    break;
                case 2:
                    atualizarProd();
                    break;
                case 3:
                    String codigoMaquina;
                    do {
                        System.out.println("Introduza o código da máquina que quer eliminar");
                        codigoMaquina = scanner.next();
                        if (getMaquinaByNome(codigoMaquina) == null) {
                            System.out.println("A máquina não existe");
                        } else {
                            eliminarMaquina(getMaquinaByNome(codigoMaquina));
                            System.out.println("MÁQUINA ELIMINADA COM SUCESSO!");
                            break;
                        }
                    } while (getMaquinaByNome(codigoMaquina) == null);

                    break;
                case 4:
                    guardarInfoMaquinas("maquinas.txt");
                    break;

                case 5:
                    ordenarAlfabeticamente();
                    break;
                case 6:
                    ordenarProdutividadeSemanalmente();
                    break;
                case 7:
                    int dia;
                    do {
                        dia = menuDiasUtilizador();
                        if (dia != 0) {
                            String[][] producaoDia = getProducaoDia(dia);
                            if (producaoDia[0][0] != null) { //se tiver algum dado guardado, significa que leu com sucesso
                                System.out.println("DIA DE PRODUÇÃO ADICIONADO Á MEMÓRIA COM SUCESSO!");
                            } else {
                                System.out.println("ERRO AO ADICIONAR DIA DE MEMÓRIA Á PRODUÇÃO. NÃO EXISTE FICHEIRO DESSE DIA NO SISTEMA");
                                System.out.println("TENTE OUTRO DIA, OU VOLTE AO MENU!");
                            }
                            atualizarProducaoDiav2(producaoDia, dia);
                        }
                    } while (dia != 0);

                    break;
                case 8:
                    mostrarTodaInfo();
                    break;
                case 9:
                    listarMaqCapProdInf30();
                    break;
                case 10:
                    guardarInfoExistentMemoria();
                    break;
                case 11:
                    determinarMelhorPiorDiaProducao();
                    break;
                case 12:
                    guardarProducaoSemanal();
                    break;
                case 13:
                    atualizarProdutividade();
                    break;
                case 14:
                    recuperarMemoria();
                    break;
            }
        } while(opcao != 0 );
    }

    public static void carregarMaquinas() {
        if (ficheiroExiste("maquinas.txt")) {
            try {
                FileReader fich = new FileReader(tempPath + "maquinas.txt");
                BufferedReader lerFich = new BufferedReader(fich);

                int count = 0;

                String linha = lerFich.readLine();

                while (linha != null) {
                    String[] dados = linha.split(",");

                    String codMaquina = "";
                    int[] capacidadeProdutiva = new int[7];

                    for (int i = 0; i < dados.length; i++) {
                        if (i == 0) {
                            codMaquina = dados[i];
                        } else if (i > 0 && i <= 7) {
                            capacidadeProdutiva[i - 1] = Integer.parseInt(dados[i]);
                        }
                    }

                    maquinas[count] = new Maquina(codMaquina, capacidadeProdutiva);
                    count++;

                    linha = lerFich.readLine();
                }

                fich.close();
            } catch (IOException e) {
                System.err.println("Erro ao verificar se já existem máquinas.");
            }
        }
    }

    public static int getInt(String msg) {
        int retValue = 0;
        boolean isInt = false;

        System.out.print(msg);

        while (!isInt) {
            try {
                retValue = scanner.nextInt();
                isInt = true;
            } catch (InputMismatchException ex) {
                scanner.next();
                System.out.println("Número inválido. Introduza um número inteiro.");
                System.out.print(msg);
            }
        }

        return retValue;
    }

    public static String getString(String msg) {
        System.out.print(msg);
        return scanner.next();
    }

    public static boolean ficheiroExiste(String nomeFicheiro) {
        File f = new File(tempPath + nomeFicheiro);

        return f.exists();
    }

    public static void criarFicheiro(String nomeFicheiro) {
        try {
            File f = new File(tempPath + nomeFicheiro);

            f.createNewFile();
        } catch (IOException e) {
            System.out.println("Erro ao criar o ficheiro.");
        }
    }

    /**
     * Função para escrever para ficheiro, o código da máquina e capacidade produtiva diária para cada máquina existente
     * em memória.
     * @param nomeFicheiro Nome do ficheiro = "maquinas.txt"
     */
    public static void guardarInfoMaquinas(String nomeFicheiro) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(tempPath + nomeFicheiro, false))) {
            for (int i = 0; i < maquinas.length && maquinas[i] != null; i++) {
                bw.write(maquinas[i].getCodMaquina());
                for (int j = 0; j < maquinas[j].getCapacidadeProdutiva().length; j++) {
                    bw.write("," + maquinas[i].getCapacidadeProdutiva()[j]);
                }
                bw.newLine();
            }
            bw.close();
        } catch (IOException e) {
        }
    }

    /**
     * Função para mostrar ao utilizador toda a informação que está em memória referente ás máquinas.
     */
    public static void mostrarTodaInfo() {
        System.out.println("");
        System.out.printf("%10s %45s\n", "CÓD. MÁQUINA", "CAPACIDADE PRODUTIVA MÁXIMA");
        System.out.println("");
        System.out.printf("%17s %9s %9s %9s %9s %9s %9s", "[S]", "[T]", "[Q]", "[Q]", "[S]", "[S]", "[D]\n");
        System.out.println("-------------------------------------------------------------------------------------------------------------------");
        for (int i = 0; i < maquinas.length && maquinas[i] != null; i++) {
            System.out.print(maquinas[i].getCodMaquina());
            for (int j = 0; j < maquinas[j].getCapacidadeProdutiva().length; j++) {
                System.out.printf("%10s", maquinas[i].getCapacidadeProdutiva()[j]);
            }
            System.out.println("");
        }
        System.out.println("-------------------------------------------------------------------------------------------------------------------");
        System.out.println("");
        System.out.printf("%10s %45s\n", "CÓD. MÁQUINA", "PRODUTIVIDADE DIAS DA SEMANA");
        System.out.println("");
        System.out.printf("%17s %9s %9s %9s %9s %9s %9s", "[S]", "[T]", "[Q]", "[Q]", "[S]", "[S]", "[D]\n");
        System.out.println("-------------------------------------------------------------------------------------------------------------------");
        for (int i = 0; i < maquinas.length; i++) {
            if (maquinas[i] != null) {
                System.out.print(maquinas[i].getCodMaquina());
                System.out.printf("%10s %9s %9s %9s %9s %9s %9s", maquinas[i].getProducaoDiaria()[0], maquinas[i].getProducaoDiaria()[1], maquinas[i].getProducaoDiaria()[2], maquinas[i].getProducaoDiaria()[3], maquinas[i].getProducaoDiaria()[4], maquinas[i].getProducaoDiaria()[5], maquinas[i].getProducaoDiaria()[6]);

                System.out.println();
            }
        }
    }

    /**
     * Função para alterar a capacidade produtiva diária de uma máquina através do seu código 
     */
    public static void atualizarProd() {
        boolean atualizada = false;
        while (!atualizada) {
            System.out.print("Digite o número da máquina: ");
            String maquina = scanner.next();
            if (getMaquinaByNome(maquina) != null) {
                int dia = isDiaSemanaValido();
                for (int i = 0; i < maquinas.length; i++) {
                    if (maquinas[i] != null && Objects.equals(maquinas[i].getCodMaquina(), maquina)) {
                        int produtividade = isProdutividadeValida();
                        int[] diasProdutividade = maquinas[i].getCapacidadeProdutiva();
                        diasProdutividade[dia - 1] = produtividade;
                        maquinas[i].setCapacidadeProdutiva(diasProdutividade);
                        atualizada = true;
                    }

                }
            } else {
                System.out.println("A máquina " + maquina + " não existe.");
            }
        }
        System.out.println("Produtividade atualizada com sucesso!");
    }

    public static int isDiaSemanaValido() {
        boolean valido = false;

        while (!valido) {
            int dia = getInt("Digite o dia da semana (1-7 (1-Segunda, 7- Domingo)): ");
            if (dia >= 1 && dia <= 7) {
                valido = true;
                return dia;
            }

        }
        return 0;
    }

    public static int isProdutividadeValida() {
        boolean valido = false;

        while (!valido) {
            int produtividade = getInt("Digite a produtividade a ser atualizada: ");
            if (produtividade >= 0 && produtividade <= 24) {
                valido = true;
                return produtividade;
            }

        }
        return 0;
    }

    public static Maquina getMaquinaByNome(String nomeMaquina) {
        Maquina maquina = null;

        for (int i = 0; i < maquinas.length; i++) {
            if (maquinas[i] != null) {
                if (maquinas[i].getCodMaquina().equals(nomeMaquina)) {
                    maquina = maquinas[i];
                    break;
                }
            }
        }

        return maquina;
    }

    /**
     * Função para eliminar uma determinada máquina.
     * 
     * @param maquina 
     */
    public static void eliminarMaquina(Maquina maquina) {
        for (int i = 0; i < maquinas.length; i++) {
            if (maquinas[i] != null) {
                if (maquinas[i].getCodMaquina() == maquina.getCodMaquina()) {
                    maquinas[i] = null;
                }
            }
        }
    }
    /**
     * Função que mostra a informação base de uma máquina introduzida pelo utilizador
     */

    public static void mostrarInformacaoBase() {
        String nomeMaquina = getString("Introduza o nome da máquina: ");
        Maquina maquina = getMaquinaByNome(nomeMaquina);

        while (maquina == null) {
            System.out.println("Máquina inserida não existe.");
            nomeMaquina = getString("Introduza o nome da máquina: ");
            maquina = getMaquinaByNome(nomeMaquina);
        }

        System.out.println("-------------------------------------------------------------------------------------------------------------------");
        System.out.printf("%30s %10s %10s %10s %10s %10s %10s %10s", "CÓD. MÁQUINA", "[S]", "[T]", "[Q]", "[Q]", "[S]", "[S]", "[D]");
        System.out.println("");
        System.out.println("-------------------------------------------------------------------------------------------------------------------");

        System.out.printf("%30s %10s %10s %10s %10s %10s %10s %10s", maquina.getCodMaquina(), maquina.getCapacidadeProdutivaByDia(0), maquina.getCapacidadeProdutivaByDia(1), maquina.getCapacidadeProdutivaByDia(2), maquina.getCapacidadeProdutivaByDia(3), maquina.getCapacidadeProdutivaByDia(4), maquina.getCapacidadeProdutivaByDia(5), maquina.getCapacidadeProdutivaByDia(6));
        System.out.println("");
    }
    /**
     * Função que ordena toda a informação em memória por codigo de máquina (alfabeticamente)
     */

    public static void ordenarAlfabeticamente() {
        Arrays.sort(maquinas, Maquina.MaquinaNameComparator);
        System.out.println("-------------------------------------------------------------------------------------------------------------------");
        System.out.printf("%-100s", "MÁQUINAS ORDENADAS ALFABETICAMENTE");
        System.out.println("");
        System.out.println("-------------------------------------------------------------------------------------------------------------------");
        for (int i = 0; i < maquinas.length; i++) {
            if (maquinas[i] != null) {
                System.out.printf("%-100s", maquinas[i].getCodMaquina());
                System.out.println("");
            }
        }
    }

    /**
     * Função para ordenar de forma decrescente a produtividade semanal que está em memória
     */
    public static void ordenarProdutividadeSemanalmente() {
        Arrays.sort(maquinas, Maquina.MaquinaProdutividadeSemComparator);
        System.out.println("-------------------------------------------------------------------------------------------------------------------");
        System.out.printf("%-100s", "MÁQUINAS ORDENADAS POR PRODUTIVIDADE SEMANAL");
        System.out.println("");
        System.out.println("-------------------------------------------------------------------------------------------------------------------");
        for (int i = 0; i < maquinas.length; i++) {
            if (maquinas[i] != null) {
                System.out.printf("%-100s", maquinas[i].getCodMaquina());
                System.out.println("");
            }
        }
    }

    /**
     * Função que mostra um menu e devolve a opção escolhida.
     * 
     * @return 
     */
    public static int menuDiasUtilizador() {
        int numDia = 0;
        do {
            System.out.println("Selecione o dia que quer através do seu número");
            System.out.println("0 - Sair");
            System.out.println("1 - Segunda - Feira");
            System.out.println("2 - Terça - Feira");
            System.out.println("3 - Quarta - Feira");
            System.out.println("4 - Quinta - Feira");
            System.out.println("5 - Sexta - Feira");
            System.out.println("6 - Sábado ");
            System.out.println("7 - Domingo ");
            numDia = getInt("");
            if (numDia < 0 || numDia > 7) {
                System.out.println("Opção inválida");
            }
        } while (numDia < 0 || numDia > 7);
        return numDia;
    }

    /**
     * Função que atualiza a produção diária em memória.
     */
    public static void preencherProducaoDiaria() {
        atualizarProducaoDiav2(getProducaoDia(1), 1);
        atualizarProducaoDiav2(getProducaoDia(2), 2);
        atualizarProducaoDiav2(getProducaoDia(3), 3);
        atualizarProducaoDiav2(getProducaoDia(4), 4);
        atualizarProducaoDiav2(getProducaoDia(5), 5);
        atualizarProducaoDiav2(getProducaoDia(6), 6);
        atualizarProducaoDiav2(getProducaoDia(7), 7);
    }

    /**
     * Função que devolve a produção de um determinado dia.
     * 
     * @param numDia
     * @return 
     */
    public static String[][] getProducaoDia(int numDia) {
        String dia = "";
        switch (numDia) {
            case 1:
                dia = "segunda";
                break;
            case 2:
                dia = "terca";
                break;
            case 3:
                dia = "quarta";
                break;
            case 4:
                dia = "quinta";
                break;
            case 5:
                dia = "sexta";
                break;
            case 6:
                dia = "sabado";
                break;
            case 7:
                dia = "domingo";
                break;
        }
        String[][] informacoes = new String[80][2];
        try {
            FileReader ficheiro = new FileReader(tempPath + dia + ".txt");
            BufferedReader lerFicheiro = new BufferedReader(ficheiro);
            String linha = lerFicheiro.readLine();
            String separador = ",";

            int count = 0;

            while (linha != null) {
                separador = linha;
                String[] dados = separador.split(",");
                informacoes[count][0] = dados[0];
                informacoes[count][1] = dados[1];
                count++;
                linha = lerFicheiro.readLine();
            }
        } catch (IOException e) {
            System.err.print("");
        }
        return informacoes;
    }

    /**
     * Função para atualizar produção diária V2.
     * 
     * @param producaoDia
     * @param numDia
     * @return 
     */
    public static long atualizarProducaoDiav2(String[][] producaoDia, int numDia) {
        Arrays.sort(maquinas, Maquina.MaquinaNameComparator);
        Cronometro cronometro = new Cronometro();
        cronometro.start();
        int producao; //variável que guarda o valor da produção do dia
        int z = 0; //indíce do array producaoDia;
        int i = 0; //indíce do array de objetos maquinas;
        while (producaoDia[z][0] != null) {
            if (maquinas[i] != null && producaoDia[z][0] != null) {
                if (maquinas[i].getCodMaquina().equalsIgnoreCase(producaoDia[z][0])) {
                    producao = parseInt(producaoDia[z][1]);
                    maquinas[i].setProducaoDiaria(numDia, producao);//se encontrar máquina vai para a próxima máquina que estava no ficheiro e continua a comparar com as máquinas.
                    z++;
                }

            }
            i++;
            if (i == 80) { //se i = 80, significa que chegou ao número máximo de máquinas permitidas, logo, não precisa de fazer mais testes.
                break;
            }
        }
        cronometro.stop();
        return cronometro.getNanoSegundos();
    }

    /**
     * Função para atualizar a produção diária.
     * 
     * Sugestão de Melhoria:
     * No segundo for em vez de compara "z menor que maquinas.length" comparava "z menor que producaoDia.length".
     * Não utilizava a variável producao, menos esse espaço de memória ocupado
     * Ordenava o array maquinas pelo nome e ao primeiro null que encontrasse saltava logo fora do ciclo for
     * 
     * @param producaoDia
     * @param numDia
     * @return 
     */
    public static long atualizarProducaoDia(String[][] producaoDia, int numDia) {
        Cronometro cronometro = new Cronometro();
        cronometro.start();
        int producao;
        for (int i = 0; i < maquinas.length; i++) {
            for (int z = 0; z < maquinas.length; z++) {
                if (maquinas[i] != null && producaoDia[z][0] != null) {
                    if (maquinas[i].getCodMaquina().equalsIgnoreCase(producaoDia[z][0])) { //se o código da máquina no array de objetos Maquinas for igual ao código da máquina do ficheiro, encontramos uma correspondência
                        producao = parseInt(producaoDia[z][1]);
                        maquinas[i].setProducaoDiaria(numDia, producao);
                    }
                }
            }
        }
        cronometro.stop();
        return cronometro.getNanoSegundos();
    }

    /**
     * Função para determinar qual foi o melhor dia de produção e o pior dia de
     * produção.
     */
    public static void determinarMelhorPiorDiaProducao() {
        String[][] rankingProducao = new String[80][3];
        int melhorDia;
        int piorDia;

        //Determinar melhor dia de produção
        for (int i = 0; i < maquinas.length; i++) {
            melhorDia = 0;
            piorDia = 25;
            if (maquinas[i] != null) {
                rankingProducao[i][0] = maquinas[i].getCodMaquina();
                rankingProducao[i][1] = "";
                rankingProducao[i][2] = "";
                //Determinar qual a produção do melhor dia
                if (maquinas[i].getProducaoSegunda() > melhorDia) {
                    melhorDia = maquinas[i].getProducaoSegunda();
                }
                if (maquinas[i].getProducaoTerca() > melhorDia) {
                    melhorDia = maquinas[i].getProducaoTerca();
                }
                if (maquinas[i].getProducaoQuarta() > melhorDia) {
                    melhorDia = maquinas[i].getProducaoQuarta();
                }
                if (maquinas[i].getProducaoQuinta() > melhorDia) {
                    melhorDia = maquinas[i].getProducaoQuinta();
                }
                if (maquinas[i].getProducaoSexta() > melhorDia) {
                    melhorDia = maquinas[i].getProducaoSexta();
                }
                if (maquinas[i].getProducaoSabado() > melhorDia) {
                    melhorDia = maquinas[i].getProducaoSabado();
                }
                if (maquinas[i].getProducaoDomingo() > melhorDia) {
                    melhorDia = maquinas[i].getProducaoDomingo();
                }

                //Determinar quais dias tiveram a mesma produção que a produção do melhor dia
                if (maquinas[i].getProducaoSegunda() == melhorDia) {
                    rankingProducao[i][1] = rankingProducao[i][1] + " Segunda-Feira | ";
                }
                if (maquinas[i].getProducaoTerca() == melhorDia) {
                    rankingProducao[i][1] = rankingProducao[i][1] + " Terça-Feira |";
                }
                if (maquinas[i].getProducaoQuarta() == melhorDia) {
                    rankingProducao[i][1] = rankingProducao[i][1] + " Quarta-Feira |";
                }
                if (maquinas[i].getProducaoQuinta() == melhorDia) {
                    rankingProducao[i][1] = rankingProducao[i][1] + " Quinta-Feira |";
                }
                if (maquinas[i].getProducaoSexta() == melhorDia) {
                    rankingProducao[i][1] = rankingProducao[i][1] + " Sexta-Feira |";
                }
                if (maquinas[i].getProducaoSabado() == melhorDia) {
                    rankingProducao[i][1] = rankingProducao[i][1] + " Sábado |";
                }
                if (maquinas[i].getProducaoDomingo() == melhorDia) {
                    rankingProducao[i][1] = rankingProducao[i][1] + " Domingo |";
                }

                //Determinar qual a produção do pior dia
                if (maquinas[i].getProducaoSegunda() < piorDia) {
                    piorDia = maquinas[i].getProducaoSegunda();
                }
                if (maquinas[i].getProducaoTerca() < piorDia) {
                    piorDia = maquinas[i].getProducaoTerca();
                }
                if (maquinas[i].getProducaoQuarta() < piorDia) {
                    piorDia = maquinas[i].getProducaoQuarta();
                }
                if (maquinas[i].getProducaoQuinta() < piorDia) {
                    piorDia = maquinas[i].getProducaoQuinta();
                }
                if (maquinas[i].getProducaoSexta() < piorDia) {
                    piorDia = maquinas[i].getProducaoSexta();
                }
                if (maquinas[i].getProducaoSabado() < piorDia) {
                    piorDia = maquinas[i].getProducaoSabado();
                }
                if (maquinas[i].getProducaoDomingo() < piorDia) {
                    piorDia = maquinas[i].getProducaoDomingo();
                }

                //Determinar quais dias tiveram a mesma produção que a produção do pior dia
                if (maquinas[i].getProducaoSegunda() == piorDia) {
                    rankingProducao[i][2] = rankingProducao[i][2] + " Segunda-Feira |";
                }
                if (maquinas[i].getProducaoTerca() == piorDia) {
                    rankingProducao[i][2] = rankingProducao[i][2] + " Terça-Feira |";
                }
                if (maquinas[i].getProducaoQuarta() == piorDia) {
                    rankingProducao[i][2] = rankingProducao[i][2] + " Quarta-Feira |";
                }
                if (maquinas[i].getProducaoQuinta() == piorDia) {
                    rankingProducao[i][2] = rankingProducao[i][2] + " Quinta-Feira |";
                }
                if (maquinas[i].getProducaoSexta() == piorDia) {
                    rankingProducao[i][2] = rankingProducao[i][2] + " Sexta-Feira |";
                }
                if (maquinas[i].getProducaoSabado() == piorDia) {
                    rankingProducao[i][2] = rankingProducao[i][2] + " Sábado |";
                }
                if (maquinas[i].getProducaoDomingo() == piorDia) {
                    rankingProducao[i][2] = rankingProducao[i][2] + " Domingo |";
                }

                if (melhorDia == 0) {//se a produção do melhorDia for igual a 0, significa que a máquina não produziu durante a semana.
                    rankingProducao[i][1] = "SEM DADOS DE PRODUÇÃO";
                    rankingProducao[i][2] = "SEM DADOS DE PRODUÇÃO";
                }
                if (melhorDia == piorDia && melhorDia != 0) {
                    rankingProducao[i][1] = "PRODUÇÃO IGUAL TODOS OS DIAS --> SEM MELHOR DIA DE PRODUÇÃO";
                    rankingProducao[i][2] = "PRODUÇÃO IGUAL TODOS OS DIAS --> SEM PIOR DIA DE PRODUÇÃO";
                }
            }
        }

        //Imprimir ranking de Produção
        System.out.println("MELHOR E PIOR DIA DE PRODUÇÃO DE CADA MÁQUINA");
        System.out.println("_______________________________________________________________________________________________________________________");
        for (int i = 0; i < rankingProducao.length; i++) {
            if (rankingProducao[i][0] != null) {
                System.out.println("Máquina : [" + rankingProducao[i][0] + "]");
                System.out.println("Melhor Dia de Produção: |" + rankingProducao[i][1]);
                System.out.println("Pior Dia de Produção :  |" + rankingProducao[i][2]);
                System.out.println("_______________________________________________________________________________________________________________________");

            }
        }
    }

    /**
     * Função que altera a produtividade das máquinas num determinado dia da semana. Não tenho nada a alterar nesta função, ela está sucinta
     * e eficaz
     */
    public static void atualizarProdutividade() {

        int dia = 0;
        int produtividade = 0;
        String codMaquina = "";
        String fim = "";
        while (!Objects.equals(fim, "000")) {
            System.out.println("Pressione ENTER para continuar. Para terminar a inserção de dados digite '000'");
            fim = scanner.nextLine();
            if (Objects.equals(fim, "000")) {
                break;
            }
            dia = isDiaSemanaValido();
            produtividade = isProdutividadeValida();
            codMaquina = isMaquinaValida();
            getMaquinaByNome(codMaquina).setProducaoDiaria(dia, produtividade);
        }

    }

    public static String isMaquinaValida() {
        boolean valida = false;
        while (!valida) {
            System.out.print("Digite o número da máquina: ");
            String maquina = scanner.next();
            if (getMaquinaByNome(maquina) != null) {
                valida = true;
                return maquina;
            } else {
                System.out.println("A máquina " + maquina + " não existe.");
            }
        }
        return "";
    }

    /**
     * Função que recupera para a memória a informação do ficheiro "infoMemoria.txt"
     * @throws IOException - trata do lançamendo de uma exceção
     */
    public static void recuperarMemoria() throws IOException {
        Ficheiro ficheiro = new Ficheiro();
        String nomeFx = "infoMemoria.txt";
        String codMaquina = "";
        int[] capacidadeProdutiva = new int[7];
        String linha = "";
        if (ficheiroExiste(nomeFx)) {
            try {
                FileReader fich = new FileReader(tempPath + nomeFx);
                BufferedReader lerFich = new BufferedReader(fich);

                int count = 0;
                int linhas = ficheiro.linhasFich(nomeFx);
                int totalMaquinas = linhas / 11;
                for (int i = 0; i < totalMaquinas; i++) {
                    for (int j = 0; j < 11; j++) {
                        linha = lerFich.readLine();
                        if (j == 0 && i == 0) {
                            String[] linhaFix = linha.split("﻿");
                            codMaquina = linhaFix[1];
                        } else if (j == 0) {
                            codMaquina = linha;
                        } else if (j > 1 && j < 9) {
                            String[] linhaSplit = linha.split(":");
                            String[] capProd = linhaSplit[1].split("\\|");
                            getMaquinaByNome(codMaquina).setProducaoDiaria((j - 1), Integer.parseInt(capProd[1]));
                            String[] cap = capProd[0].split(" ");
                            capacidadeProdutiva[j - 2] = Integer.parseInt(cap[1]);

                        }
                    }
                    getMaquinaByNome(codMaquina).setCapacidadeProdutiva(capacidadeProdutiva);
                }
                fich.close();
                System.out.println("Memória recuperada com sucesso!\n");
            } catch (IOException e) {
                System.err.println("Erro ao verificar o ficheiro " + nomeFx);
            }
        } else {
            System.out.println("Ficheiro + " + nomeFx + " não existe ");
        }

    }

    /**
     * Função para guardar em ficheiro a produção semanal por tipo de máquina. Não mudava nada nesta função,
     * porque o código está dividido em três partes, em que cada parte é referente a escrever um ficheiro para
     * cada tipo de máquina e os métodos utilizados para escrever no ficheiro são os corretos. 
     * A alternativa a isto, seria juntar o código de cada ficheiro num só bloco de código, o que 
     * constataria como uma má prática e má apresentação.
     */
    public static void guardarProducaoSemanal() {

        //MAQUINA CORTAR
        if (!ficheiroExiste("Prod_Semanal_MAQ_CORTAR.txt")) {
            criarFicheiro("Prod_Semanal_MAQ_CORTAR.txt");
        }

        try {
            String tipoMaquina;

            Formatter fm = new Formatter(new FileWriter(tempPath + "Prod_Semanal_MAQ_CORTAR.txt", false));
            fm.format("Mapa de produção semanal das máquinas do tipo: CORTAR %n");
            fm.format("%10s %30s %30s %30s", "CÓD. MÁQUINA", "CAPAC.PROD.SEM.", "PROD.SEMANAL", "DESVIO PRODUÇÃO \r\n");
            fm.format("-------------------------------------------------------------------------------------------------------- %n");

            for (int i = 0; i < maquinas.length; i++) {
                if (maquinas[i] != null) {
                    tipoMaquina = maquinas[i].getCodMaquina().substring(0, 2);
                    if (tipoMaquina.equals("CO")) {
                        int somaCPS = 0;
                        for (int j = 0; j < maquinas[i].getCapacidadeProdutiva().length; j++) {
                            somaCPS = somaCPS + maquinas[i].getCapacidadeProdutiva(j);
                        }
                        int somaPS = maquinas[i].getProducaoSegunda() + maquinas[i].getProducaoTerca() + maquinas[i].getProducaoQuarta() + maquinas[i].getProducaoQuinta() + maquinas[i].getProducaoSexta() + maquinas[i].getProducaoSabado() + maquinas[i].getProducaoDomingo();
                        fm.format("%10s %30s %30s %30s", maquinas[i].getCodMaquina(), somaCPS, somaPS, (somaPS - somaCPS) + "\r\n");
                    }
                }
            }

            fm.format("--------------------------------------------------------------------------------------------------------");
            fm.close();
        } catch (IOException e) {
            System.out.println("Problema a guardar a informação no ficheiro Prod_Semanal_MAQ_CORTAR.txt");
        }

        //MAQUINA PRENSAR
        if (!ficheiroExiste("Prod_Semanal_MAQ_PRENSAR.txt")) {
            criarFicheiro("Prod_Semanal_MAQ_PRENSAR.txt");
        }

        try {
            String tipoMaquina;

            Formatter fm = new Formatter(new FileWriter(tempPath + "Prod_Semanal_MAQ_PRENSAR.txt", false));
            fm.format("Mapa de produção semanal das máquinas do tipo: PRENSAR %n");
            fm.format("%10s %30s %30s %30s", "CÓD. MÁQUINA", "CAPAC.PROD.SEM.", "PROD.SEMANAL", "DESVIO PRODUÇÃO \r\n");
            fm.format("-------------------------------------------------------------------------------------------------------- %n");

            for (int i = 0; i < maquinas.length; i++) {
                if (maquinas[i] != null) {
                    tipoMaquina = maquinas[i].getCodMaquina().substring(0, 2);
                    if (tipoMaquina.equals("PR")) {
                        int somaCPS = 0;
                        for (int j = 0; j < maquinas[i].getCapacidadeProdutiva().length; j++) {
                            somaCPS = somaCPS + maquinas[i].getCapacidadeProdutiva(j);
                        }
                        int somaPS = maquinas[i].getProducaoSegunda() + maquinas[i].getProducaoTerca() + maquinas[i].getProducaoQuarta() + maquinas[i].getProducaoQuinta() + maquinas[i].getProducaoSexta() + maquinas[i].getProducaoSabado() + maquinas[i].getProducaoDomingo();
                        fm.format("%10s %30s %30s %30s", maquinas[i].getCodMaquina(), somaCPS, somaPS, (somaPS - somaCPS) + "\r\n");
                    }
                }
            }

            fm.format("--------------------------------------------------------------------------------------------------------");
            fm.close();
        } catch (IOException e) {
            System.out.println("Problema a guardar a informação no ficheiro Prod_Semanal_MAQ_PRENSAR.txt");
        }

        //MAQUINA FURAR
        if (!ficheiroExiste("Prod_Semanal_MAQ_FURAR.txt")) {
            criarFicheiro("Prod_Semanal_MAQ_FURAR.txt");
        }

        try {
            String tipoMaquina;

            Formatter fm = new Formatter(new FileWriter(tempPath + "Prod_Semanal_MAQ_FURAR.txt", false));
            fm.format("Mapa de produção semanal das máquinas do tipo: FURAR %n");
            fm.format("%10s %30s %30s %30s", "CÓD. MÁQUINA", "CAPAC.PROD.SEM.", "PROD.SEMANAL", "DESVIO PRODUÇÃO \r\n");
            fm.format("-------------------------------------------------------------------------------------------------------- %n");

            for (int i = 0; i < maquinas.length; i++) {
                if (maquinas[i] != null) {
                    tipoMaquina = maquinas[i].getCodMaquina().substring(0, 2);
                    if (tipoMaquina.equals("FU")) {
                        int somaCPS = 0;
                        for (int j = 0; j < maquinas[i].getCapacidadeProdutiva().length; j++) {
                            somaCPS = somaCPS + maquinas[i].getCapacidadeProdutiva(j);
                        }
                        int somaPS = maquinas[i].getProducaoSegunda() + maquinas[i].getProducaoTerca() + maquinas[i].getProducaoQuarta() + maquinas[i].getProducaoQuinta() + maquinas[i].getProducaoSexta() + maquinas[i].getProducaoSabado() + maquinas[i].getProducaoDomingo();
                        fm.format("%10s %30s %30s %30s", maquinas[i].getCodMaquina(), somaCPS, somaPS, (somaPS - somaCPS) + "\r\n");
                    }
                }
            }

            fm.format("--------------------------------------------------------------------------------------------------------");
            fm.close();
        } catch (IOException e) {
            System.out.println("Problema a guardar a informação no ficheiro Prod_Semanal_MAQ_FURAR.txt");
        }
    }

    /**
     * Função para ler do ficheiro "config.txt" o caminho escolhido pelo utilizador
     * @return O caminho escolhido pelo utilizador 
     */
    public static String getPath() {

        String path = "";
        try {

            BufferedReader bw = new BufferedReader(new FileReader("config.txt"));
            path = bw.readLine();

        } catch (IOException e) {
            System.out.println("Erro ao ler o Path dos ficheiros !");
        }
        return (path + "\\");
    }
    /**
     * Função que cria um ficheiro de texto com todas as máquinas cuja produção semanal foi inferior a 30% da sua capacidade produtiva semanal
     * Em vez de utilizar um for para obter o total de produção diária, utilizaria a função presente na libraria de JAVA Stream.
     * O código para saber o total de produção diária seria 'Arrays.stream(producaoDiaria).sum();' que iria returnar a soma total. O resto do código não mudaria nada.
     */

    public static void listarMaqCapProdInf30() {
        if (!ficheiroExiste("maquinasProdInf30.txt")) {
            criarFicheiro("maquinasProdInf30.txt");
        }
        
        int totalCapProd;
        int totalProdDiaria;
        
        try {
            FileWriter fich = new FileWriter(tempPath + "maquinasProdInf30.txt", true);
            BufferedWriter escreverFich = new BufferedWriter(fich);
            
            for (int i = 0; i < maquinas.length; i++) {
                int[] producaoDiaria;
                totalCapProd = 0;
                totalProdDiaria = 0;
                
                if (maquinas[i] != null) {
                    for (int x = 0; x < maquinas[i].getCapacidadeProdutiva().length; x++) {
                        totalCapProd = totalCapProd + maquinas[i].getCapacidadeProdutivaByDia(x);
                    }

                    producaoDiaria = maquinas[i].getProducaoDiaria();
                    for (int y = 0; y < maquinas[i].getProducaoDiaria().length; y++) {
                        totalProdDiaria = totalProdDiaria + producaoDiaria[y];
                    }
                    
                    if (totalProdDiaria <= (totalCapProd * 0.3)) {
                        escreverFich.write(maquinas[i].getCodMaquina());
                        escreverFich.newLine();
                    }
                }
            }
            
            escreverFich.close();
        } catch (IOException e) {
            System.out.println("Erro criar ficheiro.");
        }
    }
    
    /**
     * Função que guarda toda a informação existente em memória para um ficheiro
     */

    public static void guardarInfoExistentMemoria() {
        if (!ficheiroExiste("infoMemoria.txt")) {
            criarFicheiro("infoMemoria.txt");
        }
        
        try {
            FileWriter fich = new FileWriter(tempPath + "infoMemoria.txt", true);
            BufferedWriter escreverFich = new BufferedWriter(fich);
            
            for (int i = 0; i < maquinas.length; i++) {
                if (maquinas[i] != null) {
                    escreverFich.write(maquinas[i].getCodMaquina());
                    escreverFich.newLine();
                    
                    escreverFich.write("Capacidade Produção | Produção Diária");
                    escreverFich.newLine();
                    for (int x = 0; x < 7; x++) {
                        switch(x) {
                            case 0:
                                escreverFich.write("Segunda: " + maquinas[i].getCapacidadeProdutiva(x) + "|" + maquinas[i].getProducaoSegunda());
                                break;
                            case 1:
                                escreverFich.write("Terça: " + maquinas[i].getCapacidadeProdutiva(x) + "|" + maquinas[i].getProducaoTerca());
                                break;
                            case 2:
                                escreverFich.write("Quarta: " + maquinas[i].getCapacidadeProdutiva(x) + "|" + maquinas[i].getProducaoQuarta());
                                break;
                            case 3:
                                escreverFich.write("Quinta: " + maquinas[i].getCapacidadeProdutiva(x) + "|" + maquinas[i].getProducaoQuinta());
                                break;
                            case 4:
                                escreverFich.write("Sexta: " + maquinas[i].getCapacidadeProdutiva(x) + "|" + maquinas[i].getProducaoSexta());
                                break;
                            case 5:
                                escreverFich.write("Sábado: " + maquinas[i].getCapacidadeProdutiva(x) + "|" + maquinas[i].getProducaoSabado());
                                break;
                            case 6:
                                escreverFich.write("Domingo: " + maquinas[i].getCapacidadeProdutiva(x) + "|" + maquinas[i].getProducaoDomingo());
                                break;
                        }
                        escreverFich.newLine();
                    }
                    
                    escreverFich.newLine();
                    escreverFich.write("# ------------------------------------------ #");
                    escreverFich.newLine();
                }
            }
            
            escreverFich.close();
        } catch (IOException e) {
            System.out.println("Erro criar ficheiro.");
        }
    }
}
